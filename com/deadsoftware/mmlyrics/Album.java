package com.deadsoftware.mmlyrics;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 * @author Kalter
 */

public class Album implements Screen{
    
    private final List list;
    
    public Album(){
        
        list=new List("Выберите альбом:",List.IMPLICIT,new String[]{
            
            "Born Villain",
            "The Hight End of Low",
            "Eat Me, Drink Me",
            "The Golden Age of Grotesque",
            "Holy Wood",
            "Mechanical Animals",
            "Antichrist Superstar",
            "Portrait of an American Family"
        },null);
        list.setTicker(MMLyrics.ticker);
        list.addCommand(new Command("Ок",Command.OK,0));
        list.addCommand(new Command("Назад",Command.BACK,0));
        list.setCommandListener(Menu.getCommandListener());
    }
    
    public void paint(){
        
        list.setSelectedIndex(0,true);
        MMLyrics.setScreen(list);
    }
    
    public Displayable getDisplayable(){
        
        return list;
    }
    
    public String toString(){
        
        return "/album/"+list.getString(list.getSelectedIndex())+".txt";
    }
}
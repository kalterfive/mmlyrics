package com.deadsoftware.mmlyrics;

/**
 * @author Kalter
 */

public class Encoding{
    
    public static String ANSI_UTF8(String text){
        
        StringBuffer result=new StringBuffer();
        int length=text.length();
        for(int i=0;i<length;i++){
            
            char c=text.charAt(i);
            result.append((char)(c>=0xc0&&c<=0xFF?c+0x350:c));
        }
        return result.toString();
    }
    
    private Encoding(){
        
        
    }
}
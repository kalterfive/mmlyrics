package com.deadsoftware.mmlyrics;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Kalter
 */

public class StringReader{
    
    private final InputStream mInputStream;
    
    public StringReader(InputStream inputStream){
        
        mInputStream=inputStream;
    }
    
    public String readToEnd()
            throws IOException{
        
        StringBuffer result=new StringBuffer();
        while(mInputStream.available()>0){
            
            int c=mInputStream.read();
            result.append((char)c);
        }
        return result.toString();
    }
    
    public String nextString()
            throws IOException{
        
        StringBuffer result=new StringBuffer();
        int c=mInputStream.read();
        if(c==-1)return null;
        while((c!='\r')&&(c!='\n')&&(c!=-1)){
            
            result.append((char)c);
            c=mInputStream.read();
        }
        mInputStream.read();
        return result.toString();
    }
    
    public int nextInt()
            throws IOException{
        
        return Integer.parseInt(nextString());
    }
    
    public void close()
            throws IOException{
        
        mInputStream.close();
    }
}
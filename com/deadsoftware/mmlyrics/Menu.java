package com.deadsoftware.mmlyrics;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 * @author Kalter
 */

public class Menu implements CommandListener{
    
    private static CommandListener commandListener;

    public static CommandListener getCommandListener(){
        
        return commandListener;
    }
    
    private final Screen help;
    private final Screen about;
    private final Screen album;
    private final Screen language;
    private Screen lyricMenu;
    private Screen lyric;
    
    private final List list;
    
    public Menu(){
        
        commandListener=this;
        
        help=new Help();
        about=new About();
        album=new Album();
        language=new Language();
        
        list=new List("Главное Меню",List.IMPLICIT,new String[]{
            
            "Найти песню",
            "Помощь",
            "О программе",
            "Выход"
        },null);
        list.setTicker(MMLyrics.ticker);
        list.addCommand(new Command("Выбрать",Command.OK,0));
        list.setCommandListener(this);
    }
    
    public void paint(){
        
        list.setSelectedIndex(0,true);
        MMLyrics.setScreen(list);
    }
    
    public void commandAction(Command c,Displayable d){
        
        int command=c.getCommandType();
        if(d==list)menu(command);
        else if(d==about.getDisplayable())about(command);
        else if(d==help.getDisplayable())help(command);
        else if(d==album.getDisplayable())album(command);
        else if(d==lyricMenu.getDisplayable())lyricMenu(command);
        else if(d==language.getDisplayable())language(command);
        else if(d==lyric.getDisplayable())lyric(command);
    }
    
    private void menu(int command){
        
        switch(command){
            
            case Command.OK:
                switch(list.getSelectedIndex()){
            
                    case 0:
                        album.paint();
                        break;
            
                    case 1:
                        help.paint();
                        break;
            
                    case 2:
                        about.paint();
                        break;
                
                    case 3:
                        MMLyrics.exit();
                        break;
                }
                break;
        }
    }
    
    private void about(int command){
        
        switch(command){
            
            case Command.BACK:
                paint();
                break;
        }
    }
    
    private void help(int command){
        
        switch(command){
            
            case Command.BACK:
                paint();
                break;
        }
    }
    
    private void album(int command){
        
        switch(command){
            
            case Command.BACK:
                paint();
                break;
                
            case Command.OK:
                lyricMenu=new LyricMenu(
                    getClass().getResourceAsStream(album.toString()));
                lyricMenu.paint();
                break;
        }
    }
    
    private void lyricMenu(int command){
        
        switch(command){
            
            case Command.BACK:
                album.paint();
                break;
                
            case Command.OK:
                language.paint();
                break;
        }
    }
    
    private void language(int command){
        
        switch(command){
            
            case Command.OK:
                String path="/lyric/"+
                        lyricMenu.toString()+language.toString()+".txt";
                lyric=new Lyric(getClass().getResourceAsStream(path));
                lyric.paint();
                break;
            
            case Command.BACK:
                lyricMenu.paint();
                break;
        }
    }
    
    private void lyric(int command){
        
        switch(command){
            
            case Command.BACK:
                language.paint();
                break;
        }
    }
}
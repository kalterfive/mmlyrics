package com.deadsoftware.mmlyrics;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 * @author Kalter
 */

public class Language implements Screen{
    
    private final List list;
    
    public Language(){
        
        list=new List("Выберите язык:",List.IMPLICIT,new String[]{
            
            "Английский",
            "Русский"
        },null);
        list.setTicker(MMLyrics.ticker);
        list.addCommand(new Command("Выбрать",Command.OK,0));
        list.addCommand(new Command("Назад",Command.BACK,1));
        list.setCommandListener(Menu.getCommandListener());
    }
    
    public void paint(){
        
        list.setSelectedIndex(0,true);
        MMLyrics.setScreen(list);
    }
    
    public Displayable getDisplayable(){
        
        return list;
    }
    
    public String toString(){
        
        switch(list.getSelectedIndex()){
            
            case 0:return "_en";
            case 1:return "_ru";
            default:return null;
        }
    }
}
package com.deadsoftware.mmlyrics;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;

/**
 * @author Kalter
 */

public class Help implements Screen{
    
    private final Form form;
    
    public Help(){
        
        form=new Form("Помощь");
        form.setTicker(MMLyrics.ticker);
        form.addCommand(new Command("Назад",Command.BACK,0));
        form.setCommandListener(Menu.getCommandListener());
        form.append("MMLyrics - программа-сборник, содержащая все песни "+
                "Мэрилина Мэнсона, а так же их перевод на русский язык.");
    }
    
    public void paint(){
        
        MMLyrics.setScreen(form);
    }
    
    public Displayable getDisplayable(){
        
        return form;
    }
}
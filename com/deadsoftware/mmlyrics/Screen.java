package com.deadsoftware.mmlyrics;

import javax.microedition.lcdui.Displayable;

/**
 * @author Kalter
 */

interface Screen{

    void paint();
    Displayable getDisplayable();
}
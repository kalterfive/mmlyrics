package com.deadsoftware.mmlyrics;

import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Ticker;
import javax.microedition.midlet.MIDlet;

/**
 * @author Kalter
 */

public class MMLyrics extends MIDlet{
    
    public static Ticker ticker;
    private static MMLyrics self;
    private static Display display;
    
    public static void exit(){
        
        self.destroyApp(true);
    }
    
    public static void setScreen(Displayable d){
        
        display.setCurrent(d);
    }
    
    public MMLyrics(){
        
        self=this;        
        display=Display.getDisplay(this);
        ticker=new Ticker("Мэрилин Мэнсон Песни");
    }
    
    public void startApp(){
        
        new Menu().paint();
    }
    
    public void pauseApp(){
        
        
    }
    
    public void destroyApp(boolean unconditional){
        
        notifyDestroyed();
    }
}
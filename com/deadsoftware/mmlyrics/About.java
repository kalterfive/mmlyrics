package com.deadsoftware.mmlyrics;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;

/**
 * @author Kalter
 */

public class About implements Screen{
    
    private final Form form;
    
    public About(){
        
        form=new Form("О программе");
        form.setTicker(MMLyrics.ticker);
        form.addCommand(new Command("Назад",Command.BACK,0));
        form.setCommandListener(Menu.getCommandListener());
        form.append("Автор программы: Кальтер\r\n"+
                "Автор песен: Мэрилин Мэнсон\r\n"+
                "Переводчики:\r\n"+
                "Paranoic\r\n"+
                "mixer\r\n"+
                "Necrof\r\n"+
                "Posthuman\r\n"+
                "nameless\r\n"+
                "\r\n"+
                "2014 Год");
    }
    
    public void paint(){
        
        MMLyrics.setScreen(form);
    }
    
    public Displayable getDisplayable(){
        
        return form;
    }
}
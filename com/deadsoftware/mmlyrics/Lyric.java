package com.deadsoftware.mmlyrics;

import java.io.IOException;
import java.io.InputStream;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;

/**
 * @author Kalter
 */

public class Lyric implements Screen{
    
    private final Form form;
    
    public Lyric(InputStream is){
        
        String text=null;
        String title=null;
        try{
            
            StringReader sr=new StringReader(is);
            title=Encoding.ANSI_UTF8(sr.nextString());
            sr.nextString();
            text=Encoding.ANSI_UTF8(sr.readToEnd());
            sr.close();
        }catch(IOException ioe){
            
            System.out.println(ioe.getMessage());
            MMLyrics.exit();
        }
        
        form=new Form(title);
        form.setTicker(MMLyrics.ticker);
        form.addCommand(new Command("Назад",Command.BACK,0));
        form.setCommandListener(Menu.getCommandListener());
        form.append(text);
    }
    
    public void paint(){
        
        MMLyrics.setScreen(form);
    }
    
    public Displayable getDisplayable(){
        
        return form;
    }
}
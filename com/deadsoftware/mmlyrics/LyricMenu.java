package com.deadsoftware.mmlyrics;

import java.io.IOException;
import java.io.InputStream;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 * @author Kalter
 */

public class LyricMenu implements Screen{
    
    private final List list;
    
    public LyricMenu(InputStream in){
        
        String[] lyric=null;
        try{
            
            StringReader sr=new StringReader(in);
            int n=sr.nextInt();
            lyric=new String[n];
            for(int i=0;i<n;i++)lyric[i]=sr.nextString();
            sr.close();
        }catch(IOException ioe){
        
            System.out.println(ioe.getMessage());
            MMLyrics.exit();
        }
        
        list=new List("Выберите песню: ",List.IMPLICIT,lyric,null);
        list.setTicker(MMLyrics.ticker);
        list.addCommand(new Command("Выбрать",Command.OK,0));
        list.addCommand(new Command("Назад",Command.BACK,1));
        list.setCommandListener(Menu.getCommandListener());
    }
    
    public void paint(){
        
        list.setSelectedIndex(0,true);
        MMLyrics.setScreen(list);
    }
    
    public Displayable getDisplayable(){
        
        return list;
    }
    
    public String toString(){
        
        return list.getString(list.getSelectedIndex());
    }
}
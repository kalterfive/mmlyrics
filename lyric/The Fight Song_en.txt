The Fight Song

Nothing suffocates you more than
the passing of everyday human events
Isolation is the oxygen mask you make
your children breathe into survive

But I�m not a slave to a god
that doesn�t exist
I�m not a slave to a world
that doesn�t give a shit

And when we were good
you just closed your eyes
So when we are bad
we�ll scar your minds

fight, fight, fight, fight

You�ll never grow up to be a big-rock-star-celebrated-victim-of-your-fame
They�ll just cut our wrists like
cheap coupons and say that death
was on sale today

And when we were good
you just closed your eyes
So when we are bad
we�ll scar your minds

But I�m not a slave to a god
that doesn�t exist
I�m not a slave to a world
that doesn�t give a shit

the death of one is a tragedy
but the death of millions is just a statistic.

But I�m not a slave to a god
that doesn�t exist
I�m not a slave to a world
that doesn�t give a shit

fight, fight, fight, fight
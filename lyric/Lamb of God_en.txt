Lamb of God

There was Christ in the metal shell
there was blood on the pavement
The camera will make you god
That�s how Jack became sainted

If you die when there�s no one watching
Then your ratings drop and you�re forgotten
But if they kill you on their TV
You�re a martyr and a lamb of god
Nothing�s going to change
Nothing�s going to change the world

There was Lennon in the happy gun
There were words on the pavement
We were looking for the lamb of god
We were looking for Mark David

If you die when there�s no one watching
Then your ratings drop and you�re forgotten
But if they kill you on their TV
You�re a martyr and a lamb of god

Nothing�s going to change the world
Nothing�s going to change
Nothing�s going to change the world
Nothing�s going to change
The world

It took three days for him to die
The born again could buy the serial rights
Lamb of god have mercy on us
Lamb of god won�t you grant us

Nothing�s going to change the world
Nothing�s going to change
Nothing�s going to change the world
Nothing�s going to change
The world

If you die when there�s no one watching
Then your ratings drop and you�re forgotten
But if they kill you on their TV
You�re a martyr and a lamb of god
Nothing�s going to change the world
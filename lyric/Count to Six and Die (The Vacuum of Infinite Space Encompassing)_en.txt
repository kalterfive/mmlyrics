Count to Six and Die (The Vacuum of Infinite Space Encompassing)

She�s got her eyes open wide
She�s got the dirt and spit of the world
She�s got her mouth on the metal
The lips of a scared little girl

I�ve got an angel in the lobby
He�s waiting to put me in line
I won�t ask forgiveness
My faith has gone dry

She�s got her Christian prescriptures
And death has crawled in her ear
Like elevator music of songs that
She shouldn�t hear

And it spins around- 1,2,3
And we all lay down- 4,5,6
Some do it fast
And some do it better in smaller amounts
Kinderfeld

he lives inside my mouth
and tells me what to say
when he turns the trains
on he makes it go away
the hands are cracked and dirty and
the nails are beetle wings
when he turns the trains on he
unties all of the strings
the worm: �tell me something beautiful,
tell me something free,
tell me something beautiful
and I wish that I could be.�
(then I got my wings and I never even knew it,
when I was a worm, thought I couldn�t get through it)
jack: (not spoken) come, come
the toys all smell like children
and scab-knees will obey
i�ll have to kneel on broomsticks
just to make it go away.
[the inauguration of the worm]
(then I got my wings and I never even knew it,
when I was a worm, thought I couldn�t get through it)
a voice we have not yet heard: �because today
is black/because there is no turning back.
because your lies have watered me/I have become the
strongest weed� weed�
through jack�s eyes:
the taste of metal
disintegrator
three holes upon the leather belt
it�s cut and swollen
and the age is showing
boy: �There�s no one here to save ourself.�
the disintegrator: (to himself)
this is what you should fear
you are what you should fear
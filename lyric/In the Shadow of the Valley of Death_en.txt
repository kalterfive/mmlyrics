In the Shadow of the Valley of Death

We have no future
heaven wasn�t made for me
we burn ourselves to hell
as fast as it can be
and I wish that I could be the king
then I�d know that I am not alone

Maggots put on shirts
sell each other shit
sometimes I feel so worthless
sometimes I feel discarded
I wish that I was good enough
then I�d know that I am not alone

Death is policeman
Death is the priest
Death is the stereo
Death is a TV
Death is the Tarot
Death is an angel and
Death is our God
killing us all

she put the seeds in me
plant this dying tree
she�s a burning string
and I�m just the ashes